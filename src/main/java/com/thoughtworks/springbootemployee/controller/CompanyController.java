package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyMemoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/companies")
public class CompanyController {
    private static final CompanyMemoryRepository companyMemoryRepository = new CompanyMemoryRepository();

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company createCompany(@RequestBody Company company) {
        return companyMemoryRepository.createCompany(company);
    }

    @GetMapping
    public List<Company> getCompanies() {
        return companyMemoryRepository.getCompanies();
    }

    @GetMapping("/{companyId}")
    public Company getCompanyByCompanyId(@PathVariable Long companyId) {
        return companyMemoryRepository.getCompanyByCompanyId(companyId);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Long companyId){
        return companyMemoryRepository.getEmployeesByCompanyId(companyId);
    }

    @GetMapping(params = {"page","size"})
    public List<Company> getCompaniesByPageAndSize(@RequestParam Integer page,Integer size) {
        return companyMemoryRepository.getCompaniesByPageAndSize(page, size);
    }

    @PutMapping("/{companyId}")
    public Company updateCompanyName(@PathVariable Long companyId, @RequestBody Company company) {
        return companyMemoryRepository.updateCompanyName(companyId,company);
    }

    @DeleteMapping("/{companyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long companyId) {
        companyMemoryRepository.deleteCompany(companyId);

    }


}
