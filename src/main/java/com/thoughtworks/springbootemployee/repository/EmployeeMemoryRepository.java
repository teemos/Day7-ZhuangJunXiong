package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class EmployeeMemoryRepository {
    private static final List<Employee> employees = new ArrayList<>();
    private static final AtomicLong atomicId = new AtomicLong(0);

    public Employee createEmployee(Employee employee) {
        employee.setId(atomicId.incrementAndGet());
        employees.add(employee);
        return employee;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public Employee getEmployeeByEmployeeId(Long id) {
        return employees.stream().filter(employee -> employee.getId() == id).findFirst().get();
    }

    public List<Employee> getEmployeesByGender(String gender) {
        return employees.stream().filter(employee -> employee.getGender().equals(gender)).collect(Collectors.toList());
    }

    public Employee updateEmployeeAgeAndSalary(Long id, Employee employee) {
        return employees.stream()
                .filter(employeeTemp -> employeeTemp.getId().equals(id))
                .findFirst()
                .map(emp -> {
                    emp.setAge(employee.getAge());
                    emp.setSalary(employee.getSalary());
                    return emp;
                })
                .orElseThrow(() -> new RuntimeException("Employee not found with id: " + id));
    }

    public void deleteEmployee(Long id) {
        Employee employee = employees.stream()
                .filter(employeeTemp -> employeeTemp.getId().equals(id))
                .findFirst().get();
        employees.remove(employee);

    }

    public List<Employee> getEmployeesByPageAndSize(Integer page, Integer size) {
        return employees.stream()
                .skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public static List<Employee> getEmployeesByCompanyId(Long companyId){
        return employees.stream().filter(employee -> employee.getCompanyId().equals(companyId)).collect(Collectors.toList());
    }
}
