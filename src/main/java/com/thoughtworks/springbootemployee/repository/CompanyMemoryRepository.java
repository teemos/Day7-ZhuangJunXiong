package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class CompanyMemoryRepository {
    private static final List<Company> companies = new ArrayList<>();
    private static final AtomicLong atomicId = new AtomicLong(0);

    public Company createCompany( Company company) {
        company.setCompanyId(atomicId.incrementAndGet());
        companies.add(company);
        return company;
    }

    public List<Company> getCompanies() {
        return companies;
    }
    public Company getCompanyByCompanyId(Long companyId) {
        return companies.stream().filter(company -> company.getCompanyId().equals(companyId)).findFirst().get();
    }

    public List<Employee> getEmployeesByCompanyId(@PathVariable Long companyId){
        return EmployeeMemoryRepository.getEmployeesByCompanyId(companyId);
    }

    public List<Company> getCompaniesByPageAndSize(Integer page, Integer size) {
        return companies.stream()
                .skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company updateCompanyName(Long companyId, Company company) {
        return companies.stream()
                .filter(companyTemp -> companyTemp.getCompanyId().equals(companyId))
                .findFirst()
                .map(companyTemp -> {
                    companyTemp.setName(company.getName());
                    return companyTemp;
                })
                .orElseThrow(() -> new RuntimeException("Company not found with id: " + companyId));
    }

    public void deleteCompany(Long companyId) {
        Company company = companies.stream()
                .filter(employeeTemp -> employeeTemp.getCompanyId().equals(companyId))
                .findFirst().get();
        companies.remove(company);
    }
}
