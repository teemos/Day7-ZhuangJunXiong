# ORID

## O
- RESTful : RESTful is a great design style that provides a specification for interfaces.I think standardized design is very necessary for team collaboration. Only when everyone follows astandard can there be no significant disputes and improve development efficiency, which isbeneficial for the long-term development of the team
- Pair Programming:  think this is an interesting development approach, which is differentfrom traditional methods. 
- Spring Boot: I have also had experience with Spring Boot before, but not much. I mainly useother web frameworks, and today l officially entered the stage of web backend development.I am looking forward to this.

## R
Good
## I
 - Today l officially entered the stage of web backend development.I am looking forward to this.
 - Pair Programing can strengthen the relationship between two people and makethem more familiar with each other, unlike before when they may only have a rough idea ofeach other. lf everyone in the team takes turns pairing up in pairs, the cohesion of the teamwill be very strong.
 - I think standardized design is very necessary for team collaboration. Only when everyone follows astandard can there be no significant disputes and improve development efficiency, which isbeneficial for the long-term development of the team.
## D
  I will strive to master Spring Boot and Restful.